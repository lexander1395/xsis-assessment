"use strict";

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();

chai.use(chaiHttp);

describe('Movies', () => {

  describe('/GET movies', () => {
      it('it should GET all the movies', (done) => {
        chai.request(server)
            .get('/movies')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('array');
                  console.log(res.body);
              done();
            });
      });
  });
  /*
  * Test the /POST route
  */
  describe('/POST movies', () => {
      it('it should POST a movie', (done) => {
        const movie = {
          id: 1,
          tittle : "Pengabdi Setan 2 Comunion", 
          description : "dalah sebuah film horor Indonesia tahun 2022 yang disutradarai dan ditulis oleh Joko Anwar sebagai sekuel dari film tahun 2017, Pengabdi Setan.",
          rating : 7.0,
          image : "",
          created_at : "2022-08-01 10:56:31", 
          updated_at: "2022-08-13 09:30:23"
        }
        chai.request(server)
            .post('/movies')
            .send(movie)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  console.log(res.body);
              done();
            });
      });
  });

  describe('/PATCH movie(id:1)', () => {
      it('it should PATCH a movie with id equal 1', (done) => {
        const movie = {
          id: 1,
          tittle : "Pengabdi Setan 3 Comunion", 
          description : "dalah sebuah film horor Indonesia tahun 2022 yang disutradarai dan ditulis oleh Joko Anwar sebagai sekuel dari film tahun 2017, Pengabdi Setan.",
          rating : 7.0,
          image : "",
          created_at : "2022-08-01 10:56:31", 
          updated_at: "2022-08-13 09:30:23"
        }
        chai.request(server)
            .patch('/movies')
            .send(movie)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  console.log(res.body);
              done();
            });
      });
  });

  describe('/GET movie(id:1)', () => {
      it('it should GET movie with id equal 1', (done) => {
        chai.request(server)
            .get('/movies/1')
            .end((err, res) => {
                  res.should.have.status(200);
                  console.log(res.body);
              done();
            });
      });
  });

  describe('/DELETE movies', () => {
    it('it delete movies with id equal 1', (done) => {
      chai.request(server)
          .delete('/movies/1')
          .end((err, res) => {
                res.should.have.status(200);
                console.log(res.body);
            done();
          });
    });
  });
  
});