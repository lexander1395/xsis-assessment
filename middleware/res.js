"use strict";

exports.ok = function (values, res) {
    var data = {
        values: values,
    };
    res.status(200);
    res.json(values);
    res.end();
};

exports.error = function (error, res) {
    var data = {
        error: error,
    };
    res.status(500);
    res.json(data);
    res.end();
};

exports.authError = function (error, res) {
    var data = {
        error: error,
    };
    res.status(401);
    res.json(data);
    res.end();
};
