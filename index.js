//const compression = require('compression');
const express = require("express");
const cors = require("cors");
const app = express();
const port = 8080;
const bodyParser = require("body-parser");

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//app.use(compression());

require("fs")
    .readdirSync(require("path").join(__dirname, "./controllers"))
    .forEach(function (file) {
        if (file.includes("Controller.js"))
            require("./controllers/" + file)(app);
    });

app.listen(port, () =>
    console.log(`BACK END is listening at http://localhost:${port}`)
);
module.exports = app;