"use strict";
const response = require("../middleware/res");
const Movie = require("../models/MovieModel");
const { param, body, validationResult } = require('express-validator');
// constructor
module.exports = function (app) {
    app.route("/movies/:id").get(
        async (req, res) => {
            Movie.findOne(req.params.id, (err, movie) => {
                if (err) {
                    response.error(err, res);
                    return;
                }
                response.ok(movie.data, res);
            });
    });

    app.route("/movies").get( async (req, res) => {
        Movie.findAll((err, result) => {
            if (err) {
                // console.log(err)
                response.error(err, res);
            } else {
                response.ok(result, res);
            }
        });
    });

    app.route("/movies").post( 
        body('tittle').not().isEmpty().trim().escape(),
        body('description').not().isEmpty().trim().escape(),
        body('rating').isFloat({min:0}),
        body('image').trim().escape(),
        async (req, res) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
            }
            const movie = new Movie({ ...req.body });
            movie.save((err, val) => {
                if (err) {
                    response.error(err, res);
                    return;
                }
                response.ok(val, res);
            });
    });

    app.route("/movies").patch(
        body('tittle').not().isEmpty().trim().escape(),
        body('description').not().isEmpty().trim().escape(),
        body('rating').isFloat({min:0}),
        body('image').trim().escape(), 
        async (req, res) => {
            Movie.findOne(req.body.id, (err, movie) => {
                if (err) {
                    response.error(err, res);
                    return;
                }
                movie.load({ ...req.body });
                movie.save((err, result) => {
                    if (err) {
                        response.error(err, res);
                        return;
                    }
                    if (result.affectedRows == 0) {
                        response.error("Movie not found", null);
                        return;
                    }
                    response.ok(req.body, res);
                });
            });
    });

    app.route("/movies/:id").delete(
        async (req, res) => {
            Movie.remove(req.params.id, (err, result) => {
                if (err) {
                    response.error(err, res);
                    return;
                }
                response.ok(result, res);
            });
    });
};
