# xsis-assessment

to install all dependency you can install it via npm command:

npm install

use xsis_assessment.sql file to create database xsis_assessment with movies table inside.


you can edit your database connection configuration inside conn.js in db folder 

./db/conn.js


to run test you can run it via npm command:

npm run test


to run api server as a service you can run it via npm command:

npm run start