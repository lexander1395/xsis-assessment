"use strict";
const conn = require("../db/conn");

module.exports = class Movie {
    static key = "id";
    static table = "xsis_assessment.movie";
    static fields = {
        id: {},
        tittle: {},
        description: {},
        rating: {},
        image: {},
        created_at: {},
        updated_at: {},
    };
    oldId = null;
    isNew = true;
    data = {};
    constructor(model) {
        for (let key in model) {
            if (key in Movie.fields) {
                this.data[key] = model[key];
            }
        }
    }

    save = (response) => {
        if (this.isNew) {
            conn.query(
                `INSERT INTO ${Movie.table} SET ?`,
                this.data,
                (err, rows) => {
                    if (err) {
                        response(err, null);
                        return;
                    }
                    this.isNew = false;
                    response(null, this.data);
                }
            );
        } else {
            conn.query(
                `UPDATE ${Movie.table} SET ? WHERE ${Movie.key} = ?`,
                [this.data, this.oldId || this.data[Movie.key]],
                (err, rows) => {
                    if (err) {
                        response(err, null);
                        return;
                    }
                    if (rows.affectedRows == 0) {
                        response("not found", null);
                        return;
                    }
                    response(null, this.data);
                }
            );
        }
    };

    static remove = (id,response) => {
        conn.query(
            `DELETE FROM ${Movie.table} WHERE ${Movie.key} = ?`,
            id,
            (err, rows) => {
                if (err) {
                    response(err, null);
                    return;
                }
                if (rows.affectedRows == 0) {
                    response("not found", null);
                    return;
                }
                response(null, "Delete success");
            }
        );
    };

    static removeAll = (ids, response) => {
        const bindClause = Array(ids.length).fill("?").join();

        conn.query(
            `DELETE FROM ${Movie.table} WHERE ${Movie.key} IN (${bindClause}) `,
            ids,
            (err, rows) => {
                if (err) {
                    response(err, null);
                    return;
                }
                if (rows.affectedRows == 0) {
                    response("not found", null);
                    return;
                }
                response(null, ids);
            }
        );
    };

    load = (model) => {
        this.oldId = this.data[Movie.key];
        for (let key in model) {
            if (model[key] && key in Movie.fields) {
                this.data[key] = model[key];
            }
        }
    };

    static findOne = (id, response) => {
        conn.query(
            `SELECT * from ${Movie.table} WHERE ${Movie.key} = ?`,
            id,
            (err, rows) => {
                if (err) {
                    // console.log("error: ", err);
                    response(err, null);
                    return;
                }
                if (!rows[0]) {
                    response("not found", null);
                    return;
                }

                const model = new Movie(rows[0]);
                model.isNew = false;
                response(null, model);
            }
        );
    };

    static findAll = (response) => {
        conn.query(`SELECT * from ${Movie.table} `, (err, rows) => {
            if (err) {
                response(err, null);
                return;
            }
            response(null, rows);
        });
    };

    print() {
        console.log(this);
    }
};
